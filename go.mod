module bitbucket.org/schoters/APIv3-go-library/v2

go 1.16

require (
	github.com/antihax/optional v1.0.0
	golang.org/x/oauth2 v0.0.0-20210323180902-22b0adad7558
)
